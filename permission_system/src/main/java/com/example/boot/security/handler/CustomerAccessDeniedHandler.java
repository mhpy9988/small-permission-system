package com.example.boot.security.handler;

import com.example.boot.pojo.result.Result;
import com.example.boot.utils.ResponseUtil;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @description: 已经认证用户访问无权限资源处理
 * @author: mhpy
 * @date 2022/8/7 10:23
 */
@Component
public class CustomerAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) {
        //响应编码
        response.setContentType("application/json;charset=utf-8");

        ResponseUtil.out(response,Result.error("无权限访问"));
    }
}
