package com.example.boot.security.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:  登录返回结果对象
 * @author: mhpy
 * @date 2022/8/7 19:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResult {
    private Integer id;
    private String name;
    private String avatar;
    private String token;
}
