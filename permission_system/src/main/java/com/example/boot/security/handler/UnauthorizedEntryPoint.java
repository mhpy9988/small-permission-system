package com.example.boot.security.handler;

import com.example.boot.pojo.result.Result;
import com.example.boot.utils.ResponseUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @description:  未认证用户不能访问
 * @author: mhpy
 * @date 2022/8/5 22:49
 */
@Component
public class UnauthorizedEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) {

        response.setContentType("application/json;charset=utf-8");
        ResponseUtil.out(response, Result.error("未登录用户不能访问"));
    }
}
