package com.example.boot.security.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/5 21:31
 */
@Data
@NoArgsConstructor
@ToString
public class SecurityUser implements UserDetails {
    private Integer id;
    private String name;
    private String avatar;
    private String password;
    private List<String> permissions;
    public SecurityUser(Integer id,String name,String password){
        this.id = id;
       this.name = name;
       this.password = password;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // 根据自定义逻辑来返回用户权限，如果用户权限返回空或者和拦截路径对应权限不同，验证不通过
        if (!permissions.isEmpty()) {
            List<GrantedAuthority> list = new ArrayList<>();
            for (String temp : permissions) {
                GrantedAuthority au = new SimpleGrantedAuthority(temp);
                list.add(au);
            }
            return list;
        }
        return null;
    }
    @Override
    public String getPassword() {
        return this.password;
    }
    @Override
    public String getUsername() {
        return this.name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
