package com.example.boot;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author 86173
 * 主程序类
// *@SpringBootApplication这是一个springboot应用
 */
@SpringBootApplication
@MapperScan("com.example.boot.mapper")
public class MainApplication {
    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class,args);
    }
}