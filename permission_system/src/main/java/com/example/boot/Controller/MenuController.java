package com.example.boot.Controller;

import com.example.boot.pojo.entity.Menu;
import com.example.boot.pojo.result.Result;
import com.example.boot.service.MenuService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/6 22:08
 */
@CrossOrigin
@RestController
@RequestMapping("/menu")
public class MenuController {
    @Resource
    private MenuService menuService;

    @GetMapping("/get")
    public Result get(){
        List<Menu> menu = menuService.getMenu();
        return Result.success().data("item",menu);
    }
    @PreAuthorize("hasAnyAuthority('system:menu:delete')")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Integer id){
        menuService.deleteMenuById(id);
        return Result.success();
    }
    @PreAuthorize("hasAnyAuthority('system:menu:add','system:menu:edit')")
    @PostMapping("/saveOrUpdate")
    public Result saveOrUpdate(@RequestBody Menu menu){
        menuService.saveOrUpdate(menu);
        return Result.success();
    }

    @PreAuthorize("hasAuthority('system:role:bindMenu')")
    @PostMapping("/roleBindMenu/{roleId}")
    public Result roleBindMenu(@PathVariable("roleId") Integer roleId,@RequestBody Integer[] menuIds){
        menuService.roleBindMenu(roleId,menuIds);
        return Result.success();
    }
}
