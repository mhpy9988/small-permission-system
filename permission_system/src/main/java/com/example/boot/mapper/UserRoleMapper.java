package com.example.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.boot.pojo.entity.UserRole;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/6 22:01
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {
}
