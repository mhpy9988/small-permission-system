package com.example.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.boot.pojo.entity.RoleMenu;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/6 22:00
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
}
