package com.example.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.boot.pojo.entity.Role;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/6 21:59
 */
public interface RoleMapper extends BaseMapper<Role> {
}
