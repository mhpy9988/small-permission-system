package com.example.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.boot.pojo.entity.User;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/6 21:58
 */
public interface UserMapper extends BaseMapper<User> {
}
