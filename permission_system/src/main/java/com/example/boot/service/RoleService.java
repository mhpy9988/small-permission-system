package com.example.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.boot.pojo.entity.Role;

import java.util.List;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/6 22:02
 */
public interface RoleService extends IService<Role> {
    List<Integer> getCurrentRoleMenuIdList(Integer id);
}
