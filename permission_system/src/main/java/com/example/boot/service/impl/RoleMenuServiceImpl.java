package com.example.boot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.boot.mapper.RoleMenuMapper;
import com.example.boot.pojo.entity.RoleMenu;
import com.example.boot.service.RoleMenuService;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/7 9:23
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {
}
