package com.example.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.boot.pojo.entity.Menu;

import java.util.List;

 /**
  * <p>
  * 功能菜单  服务类
  * </p>
  *
  *
 * @description:
 * @author: mhpy
  *@since 2022-07-19
 * @date 2022/8/6 22:02
 */
public interface MenuService extends IService<Menu> {

    List<Menu> getMenu();

     void deleteMenuById(Integer id);


     void roleBindMenu(Integer roleId,Integer[] menuIds);

     List<String> selectPermissionValueByUserId(Integer id);

     List<Menu> getUserMenuList(Integer id);

 }
