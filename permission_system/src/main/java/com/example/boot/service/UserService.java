package com.example.boot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.boot.pojo.entity.User;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/6 21:56
 */
public interface UserService extends IService<User> {
    Page<User> getAllUser(Integer current,Integer size);

    User getUserInfo(String name);

    String[] getCurrentUserRoles(Integer id);

    void assignmentRole(Integer userId,Integer roleId);
}
