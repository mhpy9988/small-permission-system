package com.example.boot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.boot.mapper.MenuMapper;
import com.example.boot.mapper.RoleMapper;
import com.example.boot.mapper.RoleMenuMapper;
import com.example.boot.pojo.entity.Role;
import com.example.boot.pojo.entity.RoleMenu;
import com.example.boot.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/6 22:05
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
    @Resource
    private RoleMenuMapper roleMenuMapper;
    @Resource
    private MenuMapper menuMapper;

    @Override
    public List<Integer> getCurrentRoleMenuIdList(Integer id) {
        List<RoleMenu> roleMenus = roleMenuMapper.selectList(new LambdaQueryWrapper<RoleMenu>().eq(RoleMenu::getRoleId, id));
        List<Integer> roleMenuIds=new ArrayList<>();
        List<Integer> menuFatherIds = new ArrayList<>();
        if (roleMenus.size()!=0){
            for (RoleMenu roleMenu : roleMenus) {
                roleMenuIds.add(roleMenu.getMenuId());
//                如果子菜单和菜单都在当前角色拥有的菜单列表中，则去掉父菜单id,否则父菜单勾选会导致子菜单自动勾选
//                获取菜单 父id
                menuFatherIds.add(menuMapper.selectById(roleMenu.getMenuId()).getPId());
            }
//            如果菜单id列表存在父菜单id 则去掉 求差集
            roleMenuIds.removeAll(menuFatherIds);
        }
        return roleMenuIds;
    }
}
