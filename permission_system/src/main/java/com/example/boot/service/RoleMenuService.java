package com.example.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.boot.pojo.entity.RoleMenu;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/7 9:22
 */
public interface RoleMenuService extends IService<RoleMenu> {
}
