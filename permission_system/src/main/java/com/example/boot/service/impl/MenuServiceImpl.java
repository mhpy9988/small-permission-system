package com.example.boot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.boot.mapper.MenuMapper;
import com.example.boot.mapper.RoleMenuMapper;
import com.example.boot.mapper.UserRoleMapper;
import com.example.boot.pojo.entity.Menu;
import com.example.boot.pojo.entity.RoleMenu;
import com.example.boot.pojo.entity.UserRole;
import com.example.boot.service.MenuService;
import com.example.boot.service.RoleMenuService;
import com.example.boot.utils.RecursionMenu;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/6 22:04
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {
    @Resource
    private MenuMapper menuMapper;
    @Resource
    private RoleMenuMapper roleMenuMapper;
    @Resource
    private RoleMenuService roleMenuService;
    @Resource
    private UserRoleMapper userRoleMapper;

//    获取所以菜单

    @Override
    public List<Menu> getMenu() {
        List<Menu> menus = menuMapper.selectList(null);
        List<Menu> parentNode = menus.stream().filter(menu -> menu.getPId() == 0).collect(Collectors.toList());
        return RecursionMenu.recursionMenu(menus, parentNode);
    }
//  递归删除菜单

    @Override
    public void deleteMenuById(Integer id) {
        List<Integer> menuId = new ArrayList<>();
        menuId.add(id);
        List<Menu> menus = menuMapper.selectList(null);
        List<Integer> allDeleteMenuId = RecursionMenu.recursionDeleteMenu(menus, menuId);
        for (Integer integer : allDeleteMenuId) {
            roleMenuMapper.delete(new LambdaQueryWrapper<RoleMenu>().eq(RoleMenu::getMenuId, integer));
        }
        menuMapper.deleteBatchIds(allDeleteMenuId);
    }

//  角色绑定菜单

    @Override
    public void roleBindMenu(Integer roleId,Integer [] menuIds) {
        List<RoleMenu> roleMenu = new ArrayList<>();
        roleMenuMapper.delete(new LambdaQueryWrapper<RoleMenu>().eq(RoleMenu::getRoleId,roleId));
        for (Integer menuId : menuIds) {
            roleMenu.add(new RoleMenu(roleId,menuId));
        }
        roleMenuService.saveBatch(roleMenu);
    }

//  查询当前用户的权限

    @Override
    public List<String> selectPermissionValueByUserId(Integer id) {
        List<Menu> menus = userRole(id);
        List<String> permissionValues = new ArrayList<>();
        for (Menu menu : menus) {
            permissionValues.add(menu.getPermissionValue());
        }
        return permissionValues;
    }

//    查询档期用户菜单

    @Override
    public List<Menu> getUserMenuList(Integer id) {
        List<Menu> menus = userRole(id);
        List<Menu> parentNode = menus.stream().filter(menu -> menu.getPId() == 0).collect(Collectors.toList());
        return RecursionMenu.recursionMenu(menus, parentNode);
    }

//    提取公共方法  根据用户id获取角色信息，再通过角色获取对应的权限菜单列表信息（菜单id)

    private List<Menu> userRole(Integer id) {
        UserRole userRole = userRoleMapper.selectOne(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId, id));
        List<RoleMenu> list = roleMenuService.list(new QueryWrapper<RoleMenu>().eq("role_id", userRole.getRoleId()).select("menu_id"));
        List<Integer> menuIds = new ArrayList<>();
        for (RoleMenu roleMenu : list) {
            menuIds.add(roleMenu.getMenuId());
        }
        return menuMapper.selectBatchIds(menuIds);
    }
}
