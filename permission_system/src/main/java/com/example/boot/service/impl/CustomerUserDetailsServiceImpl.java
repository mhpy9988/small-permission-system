package com.example.boot.service.impl;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.example.boot.pojo.entity.User;
import com.example.boot.security.entity.SecurityUser;
import com.example.boot.service.MenuService;
import com.example.boot.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/7 8:52
 */
@Service
public class CustomerUserDetailsServiceImpl implements UserDetailsService {
    @Resource
    private UserService userService;
    @Resource
    private MenuService menuService;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = userService.getUserInfo(name);
        if (ObjectUtils.isEmpty(user)){
            throw new UsernameNotFoundException("用户名或密码错误");
        }
        List<String> authorities = menuService.selectPermissionValueByUserId(user.getId());
        SecurityUser securityUser = new SecurityUser(user.getId(),user.getName(),user.getPassword());
        securityUser.setAvatar(user.getAvatar());
        securityUser.setPermissions(authorities);
        return securityUser;
    }
}
