package com.example.boot.utils;


import com.example.boot.pojo.entity.Menu;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: 递归功能菜单
 * @author: mhpy
 * @date 2022/8/5 14:09
 */
public class RecursionMenu {

//    递归查找

    public static List<Menu> recursionMenu(List<Menu> menus, List<Menu> parentMenus){
        for (Menu parentMenu : parentMenus) {
//            找出所有数据中pid等于父级id的数据就是子级菜单
            List<Menu> menuList = menus.stream().filter(m1 -> parentMenu.getId().equals(m1.getPId())).collect(Collectors.toList());
            if (menuList.size()!=0){
                parentMenu.setChildren(menuList);
                recursionMenu(menus,menuList);
            }
        }return parentMenus;
    }
//    递归删除

    public static List<Integer> recursionDeleteMenu(List<Menu> menus,List<Integer> id){
        for (Menu parentMenu : menus) {
            if (id.contains(parentMenu.getPId())) {
                List<Integer> childIds= new ArrayList<>();
                id.add(parentMenu.getId());
                childIds.add(parentMenu.getId());
                recursionDeleteMenu(menus, childIds);
            }
        }
        return id;
    }
}
