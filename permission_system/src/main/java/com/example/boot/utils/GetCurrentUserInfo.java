package com.example.boot.utils;

import com.example.boot.security.entity.SecurityUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/8 12:41
 */
@Component
public class GetCurrentUserInfo {
    public Integer getId(){
        SecurityUser securityUser =(SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return securityUser.getId();
    }
}
