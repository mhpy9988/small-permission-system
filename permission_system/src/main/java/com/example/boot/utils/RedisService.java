package com.example.boot.utils;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/7 20:01
 */
@Component
public class RedisService {
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    public void set(String key, String value,Long timeout) {
        redisTemplate.opsForValue().set(key,value,timeout, TimeUnit.SECONDS);
    }
    public String get(String key){
        return (String) redisTemplate.opsForValue().get(key);
    }
    public void delete(String key){
        redisTemplate.delete(key);
    }
}
