package com.example.boot.utils;

import org.springframework.security.core.AuthenticationException;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/7 21:45
 */
public class MyAuthenticationException extends AuthenticationException {
    public MyAuthenticationException(String msg) {
        super(msg);
    }
    public MyAuthenticationException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
