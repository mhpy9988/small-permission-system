package com.example.boot.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/8 12:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoVo {
    private String name;
    private String avatar;
    private String sex;
    private String[] roles;
    private List<String> permissions;
    private List<MenuVo> userMenu;
}
