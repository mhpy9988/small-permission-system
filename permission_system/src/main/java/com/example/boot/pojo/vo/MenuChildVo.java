package com.example.boot.pojo.vo;

import com.example.boot.pojo.dto.Meta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/17 22:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuChildVo {
    private String path;
    private String name;
    private String component;
    private String redirect;
    private Meta meta;
}
