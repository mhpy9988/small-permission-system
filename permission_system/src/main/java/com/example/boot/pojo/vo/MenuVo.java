package com.example.boot.pojo.vo;

import com.example.boot.pojo.dto.Meta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/17 12:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuVo {
    private String path;
    private String name;
    private String component;
    private String redirect;
    private Meta meta;
    private List<MenuChildVo> children;
}
