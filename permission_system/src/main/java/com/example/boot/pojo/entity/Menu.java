package com.example.boot.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.boot.pojo.dto.Meta;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 功能菜单 
 * </p>
 *
 * @author mhpy
 * @since 2022-07-19
 */
@Getter
@Setter
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer pId;

    private String name;

    private String path;

    private String icon;

    private Integer type;

    private String permissionValue;

    private String component;

    private String redirect;

    @TableField(exist = false)
    private Meta meta;

    @TableField(exist = false)
    private List<Menu> children;

}

