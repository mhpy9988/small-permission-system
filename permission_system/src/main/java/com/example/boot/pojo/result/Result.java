package com.example.boot.pojo.result;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author: mhpy
 * @date 2022/7/12 15:29
 */
@Data
public class Result {
    private Integer code;
    private String message;
    private Boolean success;
    private Map<String,Object> data = new HashMap<>();

    private Result(){}

    public static Result success(){
        Result result=new Result();
        result.setCode(20000);
        result.setMessage("成功");
        result.setSuccess(true);
        return result;
    }
    public static Result error(){
        Result result=new Result();
        result.setCode(20001);
        result.setMessage("失败");
        result.setSuccess(false);
        return result;
    }
    public static Result error(String message){
        Result result=new Result();
        result.setCode(20001);
        result.setMessage(message);
        result.setSuccess(false);
        return result;
    }
    public Result message(String message){
        setMessage(message);
        return this;
    }
    public Result code(Integer code){
        setCode(code);
        return this;
    }
    public Result data(String key, Object value){
        this.data.put(key,value);
        return this;
    }
    public Result data(Map<String,Object> map){
        this.setData(map);
        return this;
    }
}
