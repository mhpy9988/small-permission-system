package com.example.boot.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/17 11:43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Meta {
    private String title;
    private String icon;
    private String[] roles;

    public Meta(String title, String icon) {
        this.title = title;
        this.icon = icon;
    }
}
