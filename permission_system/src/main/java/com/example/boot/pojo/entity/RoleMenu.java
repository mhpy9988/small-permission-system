package com.example.boot.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: mhpy
 * @date 2022/8/5 13:13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleMenu {
    private Integer roleId;

    private Integer menuId;
}
