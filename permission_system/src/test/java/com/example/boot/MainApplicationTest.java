package com.example.boot;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.boot.mapper.MenuMapper;
import com.example.boot.mapper.RoleMenuMapper;
import com.example.boot.pojo.entity.RoleMenu;
import com.example.boot.pojo.entity.User;
import com.example.boot.service.MenuService;
import com.example.boot.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * @description:
 * @author: mhpy
 * @date 2022/8/7 13:52
 */
@SpringBootTest
class MainApplicationTest {
    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Resource
    private MenuService menuService;
    @Resource
    private UserService userService;
    @Resource
    private RoleMenuMapper roleMenuMapper;
    @Resource
    private MenuMapper menuMapper;

    @Test
    public void testBCryptPasswordEncoder(){
        String encode = bCryptPasswordEncoder.encode("123456");
        userService.update(new User(1,"zhang","女",encode,0,"qaa"),null);
        System.out.println(encode);
        System.out.println(bCryptPasswordEncoder.matches("123456", encode));
    }

    @Test
    public void testMenuService(){
        System.out.println(menuService.selectPermissionValueByUserId(1));
    }

    @Test
    public void testRemoveAll(){
        List<RoleMenu> roleMenus = roleMenuMapper.selectList(new LambdaQueryWrapper<RoleMenu>().eq(RoleMenu::getRoleId, 2));
        List<Integer> roleMenuIds=new ArrayList<>();
        List<Integer> menuFatherIds = new ArrayList<>();
        if (roleMenus.size()!=0){
            for (RoleMenu roleMenu : roleMenus) {
                roleMenuIds.add(roleMenu.getMenuId());
//                如果子菜单和菜单都在当前角色拥有的菜单列表中，则去掉父菜单id,否则父菜单勾选会导致子菜单自动勾选
//                获取菜单 父id
                menuFatherIds.add(menuMapper.selectById(roleMenu.getMenuId()).getPId());
            }
            System.out.println(roleMenuIds+"___"+menuFatherIds);
//            如果菜单id列表存在父菜单id 则去掉 求差集
            roleMenuIds.removeAll(menuFatherIds);
        }
        System.out.println(roleMenuIds);
    }
}