# 小型权限系统
![输入图片说明](https://images.pexels.com/photos/45170/kittens-cat-cat-puppy-rush-45170.jpeg?auto=compress&cs=tinysrgb&w=600 "在这里输入图片标题")

#### 介绍
花开堪折直须折 莫待无花空折枝

#### 项目演示
管理员账号： zhang  
用户账号： 陆一叶  
密码：123456  
演示地址：http://120.26.214.98/login  

#### 系统功能
1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。

#### 技术栈
##### 前端：vue、element-ui、axios
##### 后端：SpringBoot、security、mybatis-plus、redis

#### 安装教程
1.  下载项目，解压
2.  进入项目目录 `cd small-permission-system/security-vue`
3.  安装依赖  `npm install`  下载慢可更换镜像（`npm install --registry=https://registry.npmmirror.com`）
4.  本地开发 启动项目  `npm run dev`
5.  后端代码可将项目放idea 导入相关依赖即可直接启动

#### 环境要求
JDK 1.8+  [点此下载](https://cdn.yfhl.net/java-win/jdk-8u181-windows-x64.exe)        
Mysql 8.0+  [点此下载](https://dev.mysql.com/downloads/mysql/)  
git 2.35.1+ [点击下载](https://git-scm.com/downloads)  
node 16.14.0+ [点击下载](https://nodejs.org/en/)  
Redis 5.0.14+ [点击下载](https://github.com/tporadowski/redis/releases)
