export default function hasPermission(params){
    let tag = false //是否有权限
    let codeList = JSON.parse(sessionStorage.getItem("codeList")); //sessionStorage 获取权限字段列表
    for(let i= 0 ;i<codeList.length;i++){  //遍历权限列表
        if(codeList[i] === params){  //判断传递来的权限字段是否存在于权限字段列表中
            tag=true
            break
        }
    }
    return tag;
}