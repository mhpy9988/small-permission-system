import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '主页', icon: 'dashboard' }
    }]
  },
]



/**
 * asyncRoutes
* 需要根据用户角色动态加载的路由
 */
export const asyncRoutes = [
  // {
  //   path: '/permission',
  //   component: Layout,
  //   redirect: '/permission/page',
  //   alwaysShow: true, // will always show the root menu  将始终显示根菜单
  //   name: 'Permission',
  //   meta: {
  //     title: 'Permission',
  //     icon: 'lock', 
  //     roles: ['admin', 'editor'] // you can set roles in root nav  您可以在根导航中设置角色
  //   },
  //   children: [
  //     {
  //       path: 'page',
  //       component: () => import('@/views/manage/role'),
  //       name: 'PagePermission',
  //       meta: {
  //         title: 'Page Permission',
  //         roles: ['admin'] // or you can only set roles in sub nav  或者只能在子导航中设置角色
  //       }
  //     },
  //     {
  //       path: 'directive',
  //       component: () => import('@/views/manage/role'),
  //       name: 'DirectivePermission',
  //       meta: {
  //         title: 'Directive Permission'
  //         // if do not set roles, means: this page does not require permission  如果未设置角色，则表示：此页面不需要权限
  //       }
  //     },
  //     {
  //       path: 'role',
  //       component: () => import('@/views/manage/role'),
  //       name: 'RolePermission',
  //       meta: {
  //         title: 'Role Permission',
  //         roles: ['admin']
  //       }
  //     }
  //   ]
  // }
  // {
  //   path: '/manage',
  //   component: Layout,
  //   redirect: '/manage/user',
  //   meta: {title:'权限管理',icon: 'dashboard'},
  //   children: [
  //     {
  //     path: 'user',
  //     name: 'User',
  //     component: () => import('@/views/manage/user'),
  //     meta: { title: '用户管理', icon: 'dashboard'}
  //   },
  //     {
  //     path: 'role',
  //     name: 'Role',
  //     component: () => import('@/views/manage/role'),
  //     meta: { title: '角色管理', icon: 'dashboard'}
  //   }
  // ]
  // },

  
    // 404 page must be placed at the end !!!
    { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support  需要服务支持
  mode:'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router  重置路由器
}

export default router
