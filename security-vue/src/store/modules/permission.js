
// 获取用户菜单信息
import {getInfo} from "@/api/user"
// 导入路由脚本文件中的方法
import { asyncRoutes, constantRoutes } from '@/router'

/* Layout */
import Layout from '@/layout'


/**
* 使用 meta.role 确定当前用户是否具有权限
 * @param roles
 * @param route
 */

// 权限判定
function hasPermission(roles, route) {
  
  // meta中是否有roles中的角色  包含返回true  否则false meta中没有roles则默认返回true
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
* 按递归过滤异步路由表
 * @param routes asyncRoutes
 * @param roles
 */

// 路由过滤器
export function filterAsyncRoutes(routes, roles) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }
    // children==null 则 必须 删除children 
    if(tmp.children===null){
      delete tmp.children
    }
    // 判断是否有相应权限   本人角色与菜单的角色匹配
    if (hasPermission(roles, tmp)) {
      // 菜单的component是否为layout 是则赋值Layout Layout为对象 需赋值
        if(tmp.component === 'Layout'){
            tmp.component=Layout;

        }else{
          // 加载视图
            tmp.component=loadView(tmp.component)
        }

// 子路由递归
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)

      }
      res.push(tmp)
    }
  })

  return res
}

// 视图加载  component
export const loadView = (view) => {
  // if (process.env.NODE_ENV === 'development') {
    return (resolve) => require([`@/views${view}`], resolve)
  // } else {
    // 使用 import 实现生产环境的路由懒加载
    // return () => import(`@/views/${view}`)
  // }
}


const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    // 路由器拼接 恒定路由+动态权限路由+404未定义路由  404 必须放最后  
    state.routes = constantRoutes.concat(routes,asyncRoutes)
    // state.routes = routes.concat(constantRoutes)

  }
}

const actions = {

    // 动态生成路由
  generateRoutes({ commit }, roles) {
    return new Promise(resolve => { 
        
      //查询菜单路由
      getInfo().then((res)=>{

        //存放对应权限路由信息
        let accessedRoutes;
        let userMenu=res.data.item.userMenu
        accessedRoutes = filterAsyncRoutes(userMenu, roles)
        // accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)

        // 保存路由信息到routes
        commit('SET_ROUTES', accessedRoutes)
        resolve(accessedRoutes)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}