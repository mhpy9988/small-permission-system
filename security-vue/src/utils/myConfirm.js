import { MessageBox,Message } from "element-ui";
// 删除弹框
export default function myConfirm(text){
    return new Promise((resolve,reject)=>{
        this.confirm(text,'系统提示',{
            confirmButtonText:'确定',
            cancelButtonText:'取消',
            type:'warning'
        }).then(()=>{
            resolve(true)
        }).catch(() => {
            // this.$message({
            //   type: 'info',
            //   message: '已取消'
            // });       
            reject(false)   
          })
    })
}