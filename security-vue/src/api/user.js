import request from '@/utils/request'

export function login(user) {
  return request({
    url: '/api/user/login',
    method: 'post',
    params:{name:user.name,password:user.password}
  })
}

export function getInfo(token) {
  return request({
    url: '/user/userInfo',
    method: 'get',
    params: { token  }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
export function getUser(current,size) {
  return request({
    url:  `/user/getPage/${current}/${size}`,
    method: 'get'
  })
}
export function saveOrUpdate(user) {
  return request({
    url: '/user/saveOrUpdate',
    method: 'post',
    data: user
  })
}
export function deleteUser(id) {
  return request({
    url: '/user/delete/'+id,
    method: 'delete'
  })
}
export function currentUserRole(userId) {
  return request({
    url: '/user/currentUserRole/'+userId,
    method: 'get'
  })
}
export function assignmentRole(userId,roleId) {
  return request({
    url: `/user/assignmentRole/${userId}/${roleId}`,
    method: 'post'
  })
}


