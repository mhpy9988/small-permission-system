import request from '@/utils/request'

export function getMenu() {
  return request({
    url: '/menu/get',
    method: 'get',
  })
}
export function addOrUpdate(menu) {
  return request({
    url: '/menu/saveOrUpdate',
    method: 'post',
    data:menu
  })
}
export function deleteMenu(id) {
  return request({
    url: `/menu/delete/${id}`,
    method: 'delete',
  })
}
export function roleBindMenu(roleId,menuIds) {
  return request({
    url: `/menu/roleBindMenu/${roleId}`,
    method: 'post',
    data:menuIds,
  })
}