import request from '@/utils/request'

export function getRole() {
  return request({
    url: '/role/getRole',
    method: 'get',
  })
}
export function addOrUpdateRole(role) {
  return request({
    url: '/role/addOrUpdate',
    method: 'post',
    data:role
  })
}
export function deleteRole(id) {
  return request({
    url: `/role/delete/${id}`,
    method: 'delete',
  })
}
export function currentRoleMenu(id) {
  return request({
    url: `/role/currentRoleMenu/${id}`,
    method: 'get',
  })
}