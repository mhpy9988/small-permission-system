module.exports = {

  title: '权限系统',

/**
   * @type {boolean} true | false
* @description是否显示设置右侧面板
   */

  // showSettings: true,
  /**
   * @type {boolean} true | false
   * @description Whether fix the header 是否修复标头
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar  是否在侧边栏中显示徽标
   */
  sidebarLogo: true
}
